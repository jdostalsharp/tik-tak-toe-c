#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

struct player
{
	char piece;
	bool pc;
	bool turn;
};

/* Check a piece to see if it has a full line of squares in a row
*  Returns 1 for a line, 0 for no line
*/
bool inARow(
	char** gameBoard,
	int boardSize,
	char piece,
	int column,
	int row)
{
	if (column == boardSize) 
	{
		return 1;
	}
	else
	{
		if (gameBoard[row][column] == piece)
		{
			column++;
			return (inARow(gameBoard, boardSize, piece, column, row));
		}
		else
		{
			return 0;
		}
	}
}
/* Check a piece to see if it has a full line of squares column
*  Returns 1 for a line, 0 for no line
*/
bool inAColumn(
	char** gameBoard,
	int boardSize,
	char piece,
	int row,
	int column)
{
	if (row == boardSize)
	{
		return 1;
	}
	else
	{
		if (gameBoard[row][column] == piece)
		{
			row++;
			return (inAColumn(gameBoard, boardSize, piece, row, column));
		}
		else
		{
			return 0;
		}
	}
}

/* Check the diagonal from top left to bottom right to see if there is a winner
*  Returns 1 for a line, 0 for no line
*/
bool inADiagonalTop(
	char** gameBoard,
	int boardSize,
	char piece,
	int startAt)
{
	if (startAt == boardSize)
	{
		return 1;
	}
	else
	{
		if (gameBoard[startAt][startAt] == piece)
		{
			startAt++;
			return(inADiagonalTop(gameBoard, boardSize, piece, startAt));
		}
		else
		{
			return 0;
		}
	}
}

/* Check the diagonal from top right to bottom left to see if there is a winner
*  Returns 1 for a line, 0 for no line
*/
bool inADiagonalBottom(
	char** gameBoard,
	int boardSize,
	char piece,
	int testPoint)
{
	if (testPoint == boardSize)
	{
		return 1;
	}
	else
	{
		if (gameBoard[testPoint][(boardSize - testPoint)] == piece)
		{
			testPoint++;
			return(inADiagonalBottom(gameBoard, boardSize, piece, testPoint));
		}
		else
		{
			return 0;
		}
	}
}

/* Check each row and column and the two diagonals to see if there is a winner
*  return true if there is a winner
*/
bool win(
	char** gameBoard,
	int boardSize,
	char piece)
{
	for (int i = 1; i <= boardSize - 1; i++)
	{
		int k = 1;
		int fullLine = inARow(gameBoard, boardSize, piece, k, i);
		if (fullLine == 1) 
		{
			return true;
		}
		k = 1;
		int fullColumn = inAColumn(gameBoard, boardSize, piece, k, i);
		if (fullColumn == 1)
		{
			return true;
		}
	}
	int diagonalTop = inADiagonalTop(gameBoard, boardSize, piece, 1);
	if (diagonalTop == 1)
	{
		return true;
	}
	int diagonalBottom = inADiagonalBottom(gameBoard, boardSize, piece, 1);
	if (diagonalBottom == 1)
	{
		return true;
	}
	return false;
}

/* Checks to see if there are any free positions on the game board
*  Returns true if there is available space
*/
bool freeSpace(
	char** gameBoard, 
	int boardSize)
{
	char testChar;
	bool free = false;
	for (int i = 1; i <= boardSize - 1; i++)
	{
		for (int j = 1; j <= boardSize - 1; j++)
		{
			testChar = gameBoard[i][j];
			if (testChar == ' ')
			{
				free = true;
				return free;
			}
			else
			{
				continue;
			}
		}
	}
	return free;
}

void takeTurn(
	char** gameBoard,
	int boardSize,
	struct player* player1,
	struct player* player2)
{
	srand(time(NULL));
	int row = 0;
	int column = 0;
	char piece;
	bool turn = false;
	//Get the player piece depending on who's turn it is
	do
	{
		if (player1->turn == true)
		{
			piece = player1->piece;
			player1->turn = false;
			player2->turn = true;
			turn = true;
		}
		else if (player2->turn == true)
		{
			piece = player2->piece;
			player2->turn = false;
			player1->turn = true;
			turn = true;
		}
		else
		{
			int i = rand();
			if (i % 2 == 0)
			{
				player1->turn = true;
			}
			else
			{
				player2->turn = true;
			}
		}
	} while (turn == false);

	printf("\"%c\" goes first\n", piece);
	//Change the board piece if it is within the confines of the board
	bool good = false;
	while (good == false)
	{
		printf("What Row\n");
		scanf_s("%i", &row);
		if (row > 0 && row <= boardSize)
		{
			printf("What column\n");
			scanf_s("%i", &column);
			if (column > 0 && column <= boardSize && gameBoard[row][column] == ' ')
			{
				gameBoard[row][column] = piece;
				good = true;
			}
			else
			{
				printf("That is not an acceptable move\n");
			}
		}
		fflush;
	}
}