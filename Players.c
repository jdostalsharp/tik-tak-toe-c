#include <stdbool.h>

struct player {
	char piece;
	bool pc;
	bool turn;
};


/*	Player set up
*	pc = player character
*/
void playerSetUp(
	int argc, 
	char** argv, 
	struct player *player1, 
	struct player *player2) 
{
	if (argc > 3) 
	{
		if (argv[2] == "computer") 
		{
			player1->pc = false;
		}
		else 
		{
			player1->pc = true;
		}
		if (argv[3] == "computer") 
		{
			player2->pc = false;
		}
		else 
		{
			player2->pc = true;
		}
	}
	else 
	{
		player1->pc = true;
		player2->pc = true;
	}
	player1->piece = 'X';
	player2->piece = 'O';
	player1->turn = false;
	player2->turn = false;
}