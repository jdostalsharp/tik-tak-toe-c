bool inARow(
	char** gameBoard,
	int boardSize,
	char piece,
	int i,
	int j);

bool inAColumn(
	char** gameBoard,
	int boardSize,
	char piece,
	int i,
	int j);

bool win(
	char** gameBoard,
	int boardSize,
	char piece);

void takeTurn(
	char** gameBoard,
	int boardSize,
	struct player* player1,
	struct player* player2);

bool freeSpace(
	char** gameBoard,
	int boardSize);
