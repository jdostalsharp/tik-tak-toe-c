
//Game board functions
void printGameBoard(
	char **gameBoard, 
	int sizeRows, 
	int sizeColumns);

int setSizeOfBoard(
	int argc, 
	char** argv);

void updateGameBoard(
	char** gameBoard, 
	int row, 
	int column, 
	char playerPiece);