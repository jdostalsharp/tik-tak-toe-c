#include <stdio.h>

void printGameBoard(
	char** gameBoard, 
	int sizeRows, 
	int sizeColumns) 
{
	//Length of the top and bottom of board
	int topBottomEdge = (2 * sizeRows) + 1;

	for (int i = 1; i < sizeRows; i++) 
	{
		for (int a = 0; a < topBottomEdge; a++)
		{
			printf("-");
		}
		printf("\n");
		for (int j = 1; j < sizeColumns; j++) 
		{
			printf("|%c", gameBoard[i][j]);
		}
		printf("|\n");
	}
	for (int a = 0; a < topBottomEdge; a++)
	{
		printf("-");
	}
	printf("\n");
}

int setSizeOfBoard(
	int argc, 
	char** argv) 
{
	int setSizeOfBoard;

	if (argc > 1) 
	{
		if (isdigit(argv[1])) 
		{
			int size = argv[1] - '0';
			setSizeOfBoard = size + 1;
		}
		else 
		{
			setSizeOfBoard = 4;
		}
	}
	else 
	{
		setSizeOfBoard = 4;
	}
	return setSizeOfBoard;
}

void updateGameBoard(
	char** gameBoard, 
	int row, 
	int column, 
	char playerPiece) 
{
	gameBoard[row][column] = playerPiece;
}