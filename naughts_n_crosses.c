#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include "GameBoard.h"
#include "Players.h"
#include "Game.h"

struct player 
{
	char piece;
	bool pc;
	bool turn;
};


int main(
	int argc, 
	char** argv)
{
	int sizeOfBoard;
	bool winner = false;
	bool space = true;
	struct player *player1;
	struct player *player2;

	//Set the size of board to the first argv[1]
	sizeOfBoard = setSizeOfBoard(argc, &argv);

	//Set player types depending on argv[2] and argv[3]
	playerSetUp(argc, &argv, &player1, &player2);
	
	//Initialise the game board in memory
	char **gameBoard = malloc(sizeof(char *) * sizeOfBoard);
	for (int i = 0; i < sizeOfBoard; i++) 
	{
		gameBoard[i] = malloc(sizeof(char) * sizeOfBoard);
	}

	//Blank out all spaces
	for (int i = 1; i < sizeOfBoard; i++) 
	{
		for (int j = 1; j < sizeOfBoard; j++) 
		{
			gameBoard[i][j] = ' ';
		}
	}

	printGameBoard(gameBoard, sizeOfBoard, sizeOfBoard);

	
	while (winner == false && space == true) {
		takeTurn(gameBoard, sizeOfBoard, &player1, &player2);
		printGameBoard(gameBoard, sizeOfBoard, sizeOfBoard);

		if ((win(gameBoard, sizeOfBoard, &player1->piece) == true))
		{
			winner = true;
			printf("Yay \'%c\' won, Congratulations!!!", &player1->piece);
		}
		if ((win(gameBoard, sizeOfBoard, &player2->piece) == true))
		{
			winner = true;
			printf("Yay \'%c\' won, Congratulations!!!\n", &player2->piece);
		}
		space = freeSpace(gameBoard, sizeOfBoard);
	}
	
	
	for (int i = 0; i < sizeOfBoard; i++) 
	{
		free(gameBoard[i]);
	}
	free(gameBoard);

	
	return 0;
}